<?php

class Usuario_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

  public function salvarUsuario($usuario) {
    return $this->db->insert("USUSARIOS_WEB",$usuario);
  }

  public function buscarUsuario(){
      $query = $this->db->get('USUSARIOS_WEB');
      return $query->result();
  }

  public function buscarUsuarioPorId($id) {
      $query = $this->db->get_where('USUSARIOS_WEB', array('N_ID_USU' => $id));
      return $query->row_object();
  }
}

?>
