<?php
/**
 * Description of Produto_model
 */
class Membros_model extends CI_Model {

  public $N_USER_MEM;
  public $C_NOME_MEM;
  public $C_EMAIL_MEM;
  public $C_FONE_MEM;
  public $C_ENDERE_MEM;
  public $C_INFORM_MEM;

  public function __construct() {
    parent::__construct();
  }

  public function salvarMembro($dados) {
    return $this->db->insert("MEMBROS_WEB",$dados);
  }

  public function buscarMembros(){
    $query = $this->db->select('*')
                  ->from('MEMBROS_WEB m')
                  ->order_by('m.C_NOME_MEM','asc')
                  ->get();
      return $query->result();
  }

  public function buscarMembroPorId($id) {
    $query = $this->db->select('*')
                  ->from('MEMBROS_WEB m')
                  ->where('m.N_ID_MEM = '.$id)
                  ->orderby('m.C_NOME_MEM')
                  ->get();
    return $query;
  }

  public function buscarMembrosPorNome($nome) {
    $query = $this->db->select('*')
                  ->from('MEMBROS_WEB m')
                  ->where('m.C_NOME_MEM = '.$nome)
                  ->get();
    return $query;
  }
}

?>
