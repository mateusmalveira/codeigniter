<div class="card">
<div class="card-header bg-primary text-white font-weight-bold">
Usuários
</div>
<div class="card-body" style="padding:0;">
	<table class="table table-hover" style="">
		<thead>
			<tr>
			<th>#</th>
			<th>Nome</th>
			<th>Email</th>
			<th>Fone</th>
			<th>Action</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($membros as $key=>$membro) : ?>
			<tr>
				<td><?php echo ++$key ?></td>
				<td><?php echo $membro->C_NOME_MEM ?></td>
				<td><?php echo $membro->C_EMAIL_MEM ?></td>
				<td><?php echo $membro->C_FONE_MEM ?></td>
				<td><a href="?<?php echo $membro->C_EMAIL_MEM ?>" class="btn btn-primary text-white"><i class="fas fa-search"></i></a></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
</div>
