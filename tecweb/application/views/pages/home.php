 <script>
    var element = document.getElementById("cod");
    element.classList.remove("container");
    element.classList.remove("py-5");   

</script>
 
 <main role="main">
    	<!-- carousel lindão que não é um carousel -->
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img class="d-block w-100" src="<?=base_url("static/img/equipe.jpg");?>" style='height:100vh' alt="First slide">
            <div class="container">
              <div class="carousel-caption text-left">
                <h1>Atividade 3 - Interação ao framework CodeIgniter</h1>
                <p>Desenvolvido para a disciplina Tecnologias Web (Universidade Federal do Ceará - Campus Sobral).</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container marketing pt-4 pb-3">

        <!-- Tecnologias utilizadas -->
        <div class="row">
          <div class="col-lg-4">
            <img class="rounded-circle mx-auto d-block" src="<?=base_url("static/img/bootstrap.png");?>" alt="Generic placeholder image" width="140" height="140">
            <h2 class='text-center'>Bootstrap</h2>
            <p class='text-justify'>Build responsive, mobile-first projects on the web with the world's most popular front-end component library.</p>
          </div>
          <div class="col-lg-4">
            <img class="rounded-circle mx-auto d-block" src="<?=base_url("static/img/codeigniter.jpg");?>" alt="Generic placeholder image" width="140" height="140">
            <h2 class='text-center'>CodeIgniter</h2>
            <p class='text-justify'>CodeIgniter is a powerful PHP framework with a very small footprint, built for developers who need a simple and elegant toolkit to create full-featured web applications.</p>
          </div>
          <div class="col-lg-4">
            <img class="rounded-circle mx-auto d-block" src="<?=base_url("static/img/php_logo.png");?>" alt="Generic placeholder image" width="140" height="140">
            <h2 class='text-center'>PHP</h2>
            <p class='text-justify'>PHP: Hypertext Preprocessor is a server-side scripting language designed for web development but also used as a general-purpose programming language.</p>
          </div>
        </div>
      </div>


      <!-- Direitos autorais -->
      <footer class="footer bg-dark pt-3 pb-3 text-light text-center">
        <div class="container">
          <p>&copy; 2018 Equipe das Delícias, Inc. &middot; <img style='width:1.5em' src='<?=base_url("static/img/orangejuice.png");?>'></p>
        </div>
      </footer>
    </main>
