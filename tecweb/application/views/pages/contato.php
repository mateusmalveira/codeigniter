
	<div class="card">
		<div class="card-header bg-primary text-white font-weight-bold">
			<?=$title?>
		</div>
		<div class="card-body">
            	<form action="#" method="POST" enctype="text/plain" >
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Nome:</label>
                        <div class="col-sm-10">
                        <input id="name" name="name" type="text" value="" placeholder="Insira seu nome" class="form-control" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-2 col-form-label">Email:</label>
                        <div class="col-sm-10">
                        <input id="email" name="email" type="email" value="" placeholder="seuemail@email.com.br" class="form-control" required>
                        </div>
                    </div>
                    <fieldset class="form-group">
	                    <div class="row">
	                        <legend class="col-form-label col-sm-2 pt-0">Destinatário:</legend>
	              <div class="col-sm-10">
										 <div class="form-select">
										<select name='destiny'>
											<option value='todos'> Todos</option>
												<?php foreach ($membros as $membro) : ?>
														<option value="<?php echo $membro->C_EMAIL_MEM ?>"> <?php echo $membro->C_NOME_MEM ?></option>
												<?php endforeach; ?>
										</select>
										</div>
						    </div>
	                    </div>
                	</fieldset>
                    <div class="form-group row">
                        <label for="message" class="col-sm-2 col-form-label">Deixe sua mensagem:</label>
                        <div class="col-sm-10">
                        <textarea id="message" name="message" value="" rows="5" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-10">
                            <input id="submit_button" type="submit" value="Enviar" class="btn btn-primary">
                        </div>
                    </div>
                </form>
            </div>
					</div>
