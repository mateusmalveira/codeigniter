<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?=$title?></title>
    <link rel="stylesheet" href="<?=base_url("static/css/bootstrap.min.css");?>">
    <link rel="stylesheet" href="<?=base_url("static/css/fontawesome-all.min.css");?>">
    <link rel="stylesheet" href="<?=base_url("static/css/style.css");?>">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <a class="navbar-brand" href="<?=base_url("index.php/home");?>">TecWeb</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse">
      <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link <?php if($title == 'Home'){ echo 'active';    } ?>" href="<?=base_url("index.php/home");?>">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($title == 'Sobre'){ echo 'active';    } ?>" href="<?=base_url("index.php/sobre");?>">Sobre<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link <?php if($title == 'Contato'){ echo 'active';    } ?>" href="<?=base_url("index.php/contato");?>">Contato</a>
      </li>
      </ul>
      </div>
    </nav>
    <script src="<?=base_url("static/js/jquery-3.3.1.min.js")?>"></script>
	  <script src="<?=base_url("static/js/popper.min.js")?>"></script>
	  <script src="<?=base_url("static/js/bootstrap.min.js")?>"></script>

    <div class="container py-5" id='cod'>
    <?php $this->load->view("pages/".$pages); ?>
    </div>
</body>
</html>
