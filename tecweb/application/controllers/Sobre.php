<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sobre extends CI_Controller {

	public function index(){
		//Carrega o Model
		$this->load->model('Membros_model','',TRUE);
		
		$data['membros'] = $this->Membros_model->buscarMembros();
		$data['pages'] = "sobre";
		$data['title'] = "Sobre";
		$this->load->view('home',$data);
	}
}
