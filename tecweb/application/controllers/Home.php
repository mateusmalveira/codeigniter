<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Atividade 3 - Interação ao framework CodeIgniter
	 * Desenvolvido para a disciplina Tecnologias Web
	 * Universidade Federal do Ceará - Campus Sobral
	 */
	public function index(){
		$data['pages'] = "home";
		$data['title'] = "Home";
 		$this->load->view('home',$data);
	}
}
