<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contato extends CI_Controller {

	public function index(){
		//Carrega o Model
		$this->load->model('Membros_model','',TRUE);
		$this->load->model('Usuario_model','',TRUE);

		//$dados['C_LOGIN_USU'] = 'josé';
		//$dados['C_SENHA_USU'] = 'rafael';

		//$this->Usuario_model->salvarUsuario($dados);
		$data['membros'] = $this->Membros_model->buscarMembros();
		$data['pages'] = "contato";
		$data['title'] = "Contato";
		$this->load->view('home',$data);
	}
}
